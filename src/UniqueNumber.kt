package ge.lv.test.unique

import java.util.*

fun main(args: Array<String>) {
    val array = intArrayOf(2, 2, 1, 3, 1, 1)
    println(solution(array))
}

fun solution(A: IntArray): Int {
    val set = mutableSetOf<Int>()
    A.forEach {
        set.add(it)
    }
    return set.size
}