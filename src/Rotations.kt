import java.util.*

fun main(args: Array<String>) {
    val array = intArrayOf(3, 8, 9, 7, 6)

    print(Arrays.toString(solution(array, 3)))
}

fun solution(A: IntArray, K: Int): IntArray {
    val out = IntArray(A.size)
    for (i in 0 until A.size) {
        var targetIndex = i+K
        if (targetIndex>=A.size){
            targetIndex %= A.size
        }
        out[targetIndex]=A[i]
    }

    return out
}