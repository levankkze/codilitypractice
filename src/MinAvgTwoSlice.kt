package ge.lv.test.MinAvgTwoSlice

import java.util.*

fun main(args: Array<String>) {
    var A = intArrayOf(4, 2, 2, 5, 1, 5, 8)
    println(solution(A))
    A = intArrayOf(10000, -10000)
    println(solution(A))
}


fun solution(A: IntArray): Int {

//    val prefix = IntArray(A.size + 1)
//    var sum = 0
//    for (i in 0 until A.size) {
//        sum += A[i]
//        prefix[i + 1] = sum
//    }
//
//    println(Arrays.toString(prefix))

    var minAvrg = Float.MAX_VALUE
    var minAvrgindex = 0

    for (i in 0 until A.size - 1) {
        val avrgOf2 = (A[i] + A[i + 1]) / 2f
        if (minAvrg > avrgOf2) {
            minAvrg = avrgOf2
            minAvrgindex = i
        }
        if (i < A.size - 2) {
            val avrgOf3 = (A[i] + A[i + 1] + A[i + 2]) / 3f
            if (minAvrg > avrgOf3) {
                minAvrg = avrgOf3
                minAvrgindex = i
            }
        }
    }
    return minAvrgindex

}


