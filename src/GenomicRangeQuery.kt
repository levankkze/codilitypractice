package ge.lv.test.gemoic

import java.util.*

fun main(args: Array<String>) {
    var S = "CAGCCTA"
    var P = intArrayOf(2, 5, 0)
    var Q = intArrayOf(4, 5, 6)
    println(Arrays.toString(solution(S, P, Q)))
    S = "AC"
    P = intArrayOf(0, 0, 1)
    Q = intArrayOf(0, 1, 1)
    println(Arrays.toString(solution(S, P, Q)))
    S = "TC"
    P = intArrayOf(0, 0, 1)
    Q = intArrayOf(0, 1, 1)
    println(Arrays.toString(solution(S, P, Q)))
}

val factors = hashMapOf<Char, Int>('A' to 1, 'C' to 2, 'G' to 3, 'T' to 4)

fun solution(S: String, P: IntArray, Q: IntArray): IntArray {
    val chars = S.toCharArray()
    val out = IntArray(P.size)
    val sumsArrays = arrayOf<IntArray>(
        IntArray(chars.size + 1),
        IntArray(chars.size + 1),
        IntArray(chars.size + 1),
        IntArray(chars.size + 1)
    )

    val factorCounters = arrayOf(0, 0, 0, 0)

    for (i in 0 until chars.size) {
        val factor = factors[chars[i]] ?: continue
        factorCounters[factor - 1]++
        sumsArrays.forEachIndexed { j, array ->
            array[i + 1] = factorCounters[j]
        }
    }

    for (i in 0 until P.size) {
        for (j in 1..sumsArrays.size) {
            val s = P[i]
            val e = Q[i] + 1
            val cs = sumsArrays[j - 1][s]
            val ce = sumsArrays[j - 1][e]
            val n = ce - cs
            if (n > 0) {
                out[i] = j
                break
            }
        }
    }
    return out
}