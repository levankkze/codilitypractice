package ge.lv.test.passingcar

import java.util.*

fun main(args: Array<String>) {
    val array = intArrayOf(0, 1, 0, 1, 1)
    println(solution(array))
}

fun solution(A: IntArray): Int {
    var totalSum = 0
    var sum = 0
    for (i in A.size - 1 downTo 0) {
        if (A[i] == 0) {
            totalSum += sum
            if (totalSum > 1000000000) return -1
        } else {
            sum++
        }
    }
    return totalSum
}