package ge.lv.test.Countdiv


fun main(args: Array<String>) {
    println("out: " + solution(6, 11, 2))
    println("out: " + solution(5, 11, 2))
}


fun solution(A: Int, B: Int, K: Int): Int {
    val minNum = A / K - (if (A % K == 0) 1 else 0)
    val maxNum = B / K
    return maxNum - minNum
}


