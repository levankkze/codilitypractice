package ge.lv.test.MaxCounters

import java.util.*

fun main(args: Array<String>) {
    var array = intArrayOf(3, 4, 4, 6, 1, 4, 4)
    println(Arrays.toString(solution(5, array)))
    array = intArrayOf(1)
    println(Arrays.toString(solution(1, array)))
}

fun solution(N: Int, A: IntArray): IntArray {
    var counter = IntArray(N)
    var max = 0
    var lastMax = 0
    for (a in A) {
        if (a <= N) {
            if (counter[a - 1]<lastMax){
                counter[a - 1]=lastMax
            }
            val increased = ++counter[a - 1]
            if (increased > max)
                max = increased
        }
        if (a == N + 1) {
            lastMax = max
        }
        println(Arrays.toString(counter))
    }
    for (i in 0 until N) {
        if (counter[i] < lastMax) counter[i] = lastMax
    }

    return counter
}