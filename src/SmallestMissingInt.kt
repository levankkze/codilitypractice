package ge.lv.test.smallestminssingint

import kotlin.math.abs

fun main(args: Array<String>) {
    var array = intArrayOf(1, 3, 6, 4, 1, 2)
    println(solution(array))
     array = intArrayOf(-1,-3)
    println(solution(array))
}

fun solution(A: IntArray): Int {
    val set= mutableSetOf<Int>()
    var maxVal=0
    for ((i,v) in A.withIndex()){
        set.add(v)
        if (maxVal<v) maxVal=v
    }
    if (maxVal==0) return 1
    for (i in 1..maxVal){
        if (!set.contains(i)) return i
    }
    return 1
}