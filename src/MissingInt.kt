fun main(args: Array<String>) {
    val array = intArrayOf(2, 3, 1, 5)
    println(solution(array))
    val arrayo = intArrayOf(2, 3, 1, 5, 6, 7, 9, 8)
    println(solution(arrayo))
}

fun solution(A: IntArray): Int {
    var out = 0
    for (i in 1..A.size) {
        out -= A[i - 1]
    }
    for (i in 1..A.size+1) {
        out += i
    }
    return out
}