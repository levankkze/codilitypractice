package ge.lv.test.tape

import kotlin.math.abs

fun main(args: Array<String>) {
    val array = intArrayOf(3, 1, 2, 4, 3)
    println(solution(array))
}

fun solution(A: IntArray): Int {
    val sum = A.sum()
    var min = Int.MAX_VALUE
    var lSum = A[0]
    for (i in 1 until A.size) {
        val l = lSum
        val r = sum - lSum
        val dif = abs(l - r)
        if (dif < min) min = dif
        lSum += A[i]
    }
    return min
}