package ge.lv.test.permutations

import kotlin.math.abs

fun main(args: Array<String>) {
    val array = intArrayOf(3, 1, 2, 4, 5)
    println(solution(array))
}

fun solution(A: IntArray): Int {

    var checkSum=0
    for (i in 1..A.size) {
        checkSum =checkSum or i
        checkSum =checkSum xor A[i-1]
    }

    return if (checkSum==0) 1 else 0
}