package ge.lv.test.frog

import kotlin.math.abs

fun main(args: Array<String>) {
    val array = intArrayOf(1,3,1,4,2,3,5,4)
    println(solution(5,array))
}

fun solution(X: Int, A: IntArray): Int {
    val leafs= mutableSetOf<Int>()
    for ((i,x) in A.withIndex()){
        leafs.add(x)
        if (leafs.size==X) return i
    }
    return -1
}